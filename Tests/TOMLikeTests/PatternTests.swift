// Copyright 2023-2024 Kai Oezer

import Testing
@testable import TOMLike

struct PatternTests
{
	@Test("strings")
	func strings()
	{
		let s1 = #""this is a simple string""#
		#expect(s1.wholeMatch(of: Pattern.stringWithEscapes) != nil)

		let s2 = #""this is an \"escaped\" string""#
		#expect(s2.wholeMatch(of: Pattern.stringWithEscapes) != nil)

		let s3 = #"this line contains "a simple string"."#
		#expect(s3.wholeMatch(of: Pattern.lineWithString) != nil)

		let s4 = #"this line contains "an \"escaped\" string"."#
		#expect(s4.wholeMatch(of: Pattern.lineWithString) != nil)

		let s5 = "this line contains no string"
		#expect(s5.wholeMatch(of: Pattern.stringWithEscapes) == nil)
		#expect(s5.wholeMatch(of: Pattern.lineWithString) == nil)
	}

	@Test("comments")
	func comments()
	{
		let s1 = "# a trailing comment"
		#expect(s1.wholeMatch(of: Pattern.commentOnlyLine) != nil)

		let s2 = "   # a trailing comment"
		#expect(s2.wholeMatch(of: Pattern.commentOnlyLine) != nil)

		let s3 = "bla   # a trailing comment"
		#expect(s3.wholeMatch(of: Pattern.commentOnlyLine) == nil)
		#expect(s3.wholeMatch(of: Pattern.lineWithoutStringWithComment) != nil)

		let s4 = #"bla "bla"   # a trailing comment"#
		#expect(s4.wholeMatch(of: Pattern.lineWithoutStringWithComment) == nil)
	}

	@Test("numbers")
	func numbers()
	{
		#expect("45.23".wholeMatch(of: Pattern.value_float) != nil)
		#expect("45.23".wholeMatch(of: Pattern.value_integer) == nil)
		#expect("45".wholeMatch(of: Pattern.value_integer) != nil)
		#expect("45".wholeMatch(of: Pattern.value_float) != nil)
	}
}
