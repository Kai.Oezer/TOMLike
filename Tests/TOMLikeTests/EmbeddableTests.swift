// Copyright 2022-2024 Kai Oezer

import Foundation
import Testing
@testable import TOMLike
import IssueCollection

struct EmbeddableTests
{
	@Test("embedding")
	func embedding() throws
	{
		struct TypeWithEmbeddableMembers : Codable, Equatable
		{
			var place : String
			var person1 : EmbeddableTestItem
			var person2 : EmbeddableTestItem
		}

		let archive1 = """
			place = "house"

			[person1]
			Tom : 3

			[person2]
			Jerry : 4
			"""

		let example1 = TypeWithEmbeddableMembers(place: "house", person1: .init(name: "Tom", age: 3), person2: .init(name: "Jerry", age: 4))

		let archivedExample1 = try TOMLikeCoder.encode(example1)
		#expect(archivedExample1 == archive1)

		var decodingIssues = IssueCollection()
		let decodedArchive1 = try TOMLikeCoder.decode(TypeWithEmbeddableMembers.self, from: archive1, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(decodedArchive1 == example1)
	}

}
