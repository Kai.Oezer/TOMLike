// Copyright 2022-2024 Kai Oezer

import Foundation
import TOMLike
import IssueCollection

struct SimpleKeyValuePairTestItem1 : Codable
{
	var title : String
}

struct SimpleKeyValuePairTestItem2 : Codable
{
	var key : String
	var another : String
}

struct EmbeddableTestItem : Codable, TOMLikeEmbeddable, Equatable
{
	var name : String
	var age : Int

	init(name : String, age : Int)
	{
		self.name = name
		self.age = age
	}

	init(tomlikeEmbedded archivedContent : [IndexedLine], issues : inout IssueCollection) throws
	{
		let firstLine = archivedContent[0].content
		let tokens = firstLine.components(separatedBy: ":").map{ $0.trimmingCharacters(in: .whitespaces) }
		guard tokens.count == 2, let age = Int(tokens.last!) else { throw DecodingError.dataCorrupted(.init(codingPath: [], debugDescription: "invalid format", underlyingError: nil)) }
		self.name = tokens.first!
		self.age = age
	}

	func tomlikeEmbedded() throws -> String
	{
		"\(name) : \(age)"
	}
}

struct TestData
{
	static let archive2 =
		"""
		[single]
		Johnny = 12

		[multi.0]
		Laurel = 37

		[multi.1]
		Hardy = 35
		"""

	static let archive3 =
		"""
		Type A: Tom 4
		Type A: Jerry 3
		"""

	static let archive4 =
		"""
		# This is a TOML document

		title = "TOML Example"

		[owner]
		name = "Tom Preston-Werner"
		dob = 1979-05-27T07:32:00-08:00

		[database]
		enabled = true
		ports = [ 8000, 8001, 8002 ]
		data = [ ["delta", "phi"], [3.14] ]
		temp_targets = { cpu = 79.5, case = 72.0 }

		[servers]

		[servers.alpha]
		ip = "10.0.0.1"
		role = "frontend"

		[servers.beta]
		ip = "10.0.0.2"
		role = "backend"
		"""
}
