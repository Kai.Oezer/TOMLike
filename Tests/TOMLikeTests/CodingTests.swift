// Copyright 2022-2024 Kai Oezer

import Foundation
import Testing
@testable import TOMLike
import IssueCollection

struct Coding
{
	@Test("encoding and decoding dates")
	func encodingAndDecodingDates() throws
	{
		struct SomeStruct : Codable
		{
			var d : Date
		}

		let expectedString = "d = 2022-04-06T01:35:00Z"
		let expectedDate = Calendar(identifier:.gregorian).date(from: .init(timeZone: .init(identifier: "GMT"), year:2022, month:4, day:6, hour: 1, minute: 35))!

		let archive = Archive(from: expectedString)
		#expect(archive.issues.count == 0)

		let s = try TOMLikeCoder.decode(SomeStruct.self, from: archive)
		#expect(s.d == expectedDate)

		let encodedStruct = try TOMLikeCoder.encode(SomeStruct(d: expectedDate))
		#expect(encodedStruct == expectedString)
	}

	@Test("encoding and decoding optional values")
	func encodingAndDecodingOptionalValues() throws
	{
		struct OptItems : Codable
		{
			var id : Int
			var name : String?
		}

		let expectedEncoded = "id = 4"
		let archive = Archive(from: expectedEncoded)
		#expect(archive.issues.count == 0)
		let o = try TOMLikeCoder.decode(OptItems.self, from: archive)
		#expect(o.id == 4)
		try #require(o.name == nil)
		#expect(try TOMLikeCoder.encode(o) == expectedEncoded)

		let expectedEncoded2 = "id = 5\nname = \"John\""
		let archive2 = Archive(from: expectedEncoded2)
		#expect(archive2.issues.count == 0)
		let o2 = try TOMLikeCoder.decode(OptItems.self, from: archive2)
		#expect(o2.id == 5)
		#expect(o2.name == "John")
		#expect(try TOMLikeCoder.encode(o2) == expectedEncoded2)
	}

}
