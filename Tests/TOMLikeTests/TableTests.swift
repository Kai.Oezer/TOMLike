// Copyright 2022-2024 Kai Oezer

import Testing
@testable import TOMLike
import IssueCollection

struct TableTests
{
	@Test("detecing tables 1")
	func detectingTables1() throws
	{
		let archive = Archive(from: """
			[table]
			a = 1
			b = 2
			[ another_table ]
			x = "bla"
			[table.subtable]
			c = 3
			[yet.another .  table ]
			yy = 99
			"""
		)
		try #require(archive.issues.count == 0)
		try #require(archive.tables.count == 4)
		try #require(archive.tables[0].path.length == 1)
		#expect(archive.tables[0].path.path == ["table"])
		try #require(archive.tables[1].path.length == 1)
		#expect(archive.tables[1].path.path == ["another_table"])
		try #require(archive.tables[2].path.length == 2)
		#expect(archive.tables[2].path.path == ["table", "subtable"])
		try #require(archive.tables[3].path.length == 3)
		#expect(archive.tables[3].path.path == ["yet", "another", "table"])
	}

	// swiftlint:disable:next function_body_length
	@Test("detecting tables 2")
	func detectingTables2() throws
	{
		struct SomeStruct : Codable
		{
			// swiftlint:disable:next nesting
			struct Nested1 : Codable
			{
				var aa : Int
			}
			var a : Nested1

			// swiftlint:disable:next nesting
			struct Nested2 : Codable
			{
				var ba : Int
			}
			var b : Nested2

			// swiftlint:disable:next nesting
			struct Nested3 : Codable
			{
				// swiftlint:disable:next nesting
				struct Nested4 : Codable
				{
					var ca : String
					var cb : String
					var cc : String
					var cd : String
				}
				var a : Nested4

				// swiftlint:disable:next nesting
				struct Nested5 : Codable
				{
					var cba : Int
				}
				var b : Nested5
			}
			var c : Nested3

			// swiftlint:disable:next nesting
			struct Nested6 : Codable
			{
			}
			var d : Nested6
		}

		let issues = IssueCollection()
		let archive = Archive(from:
			"""
			c.a.cd = "?"

			[a]
			aa = 1

			[b]
			ba = 2

			[c.a] # contains strings
			ca = "hello"
			cb = "how are you"
			cc = "today"

			[c.b]
			cba = 10

			[d]
			"""
		)
		try #require(archive.tables.count == 6)
		#expect(issues.count == 0)
		#expect(archive.tables[0].path.path == [String]())
		#expect(archive.tables[0].lines.count == 1)
		#expect(archive.tables[0].values.count == 0)
		#expect(archive.tables[1].path.path == ["a"])
		#expect(archive.tables[2].path.path == ["b"])
		#expect(archive.tables[3].path.path == ["c", "a"])
		#expect(archive.tables[3].lines.count == 3)
		#expect(archive.tables[3].lines[1].content == "cb = \"how are you\"")
		#expect(archive.tables[5].lines.count == 0)

		#expect(archive.tables[3].values.count == 0)
		_ = try TOMLikeCoder.decode(SomeStruct.self, from: archive)
		#expect(archive.tables[3].values.count == 4)
	}

	@Test("detecting split tables")
	func detectingSplitTables() throws
	{
		let archive = Archive(from:
			"""
			[two_part_table] # first part
			aa = 1

			[b]
			ba = 2

			[c] # contains strings
			caa = "hello"
			cab = "how are you"

			[two_part_table] # second part
			ab = 2
			ac = 3
			"""
		)
		try #require(archive.tables.count == 3)
		#expect(archive.issues.count == 0)
		#expect(archive.tables[0].path.path == ["two_part_table"])
		#expect(archive.tables[0].lines.count == 3)
	}

}
