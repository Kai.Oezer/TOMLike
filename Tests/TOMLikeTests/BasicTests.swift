// Copyright 2022-2024 Kai Oezer

import Testing
@testable import TOMLike
import IssueCollection

@Suite("Basics")
struct Basics
{
	@Test("stripping line comments")
	func strippingLineComments()
	{
		let pureComment = "# a comment line"
		#expect(pureComment.strippingTOMLikeComments.isEmpty)

		let s1 = #"another "day" "#
		#expect((s1 + pureComment).strippingTOMLikeComments == s1)

		let s2 = #"title = "string containing # hash character""#
		#expect(s2.strippingTOMLikeComments == s2)

		let s3 = "place = \"#2\" "
		#expect((s3 + pureComment).strippingTOMLikeComments == s3)

		let s4 = "number = 5 "
		#expect((s4 + pureComment).strippingTOMLikeComments == s4)

		let s5 = "array = [\"#5\", \"#16\", 14 ]"
		#expect(s5.strippingTOMLikeComments == s5)
		#expect((s5 + pureComment).strippingTOMLikeComments == s5)

		let s6 = #"bla "bla"  "oh!" , "yes "   # a trailing comment"#
		#expect(s6.strippingTOMLikeComments == #"bla "bla"  "oh!" , "yes "   "#)
	}

	@Test("encoding a simple struct")
	func encodingSimpleStruct() throws
	{
		let example = SimpleKeyValuePairTestItem2(key: "hello", another: "test")
		let archive = try TOMLikeCoder.encode(example)
		#expect(archive == "key = \"hello\"\nanother = \"test\"")
	}

	@Test("decoding key-value pair")
	func decodingKeyValuePair() throws
	{
		var decodingIssues = IssueCollection()
		let example1 = "title = \"hello world\""
		let decoded = try TOMLikeCoder.decode(SimpleKeyValuePairTestItem1.self, from: example1, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(decoded.title == "hello world")

		let example2 = "title = #adfff"
		#expect(throws: (any Error).self) {
			try TOMLikeCoder.decode(SimpleKeyValuePairTestItem1.self, from: example2, issues: &decodingIssues)
		}
	}

	@Test("decoding key-value pair with custom coding key")
	func decodingKeyValuePairWithCustomCodingKey() throws
	{
		struct SomeStruct : Codable, Equatable
		{
			var a : String
			var a_ : Int
			var a2 : Int

			// swiftlint:disable:next nesting
			enum CodingKeys : String, CodingKey
			{
				case a = "a-1"
				case a_ = "a+1"
				case a2 = "a**"
			}
		}

		let archive = "a-1 = \"flower power\"\na+1 = 9\na** = 100"
		let expected = SomeStruct(a: "flower power", a_: 9, a2: 100)
		var issues = IssueCollection()
		let decoded = try TOMLikeCoder.decode(SomeStruct.self, from: archive, issues: &issues)
		#expect(issues.count == 0)
		#expect(decoded == expected)
	}

	@Test("decoding comments")
	func decodingComments() throws
	{
		let archive = """
			# This is a full-line comment
			key = "value"  # This is a comment at the end of a line
			another = "# This is not a comment"
			"""
		var decodingIssues = IssueCollection()
		let decoded = try TOMLikeCoder.decode(SimpleKeyValuePairTestItem2.self, from: archive, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(decoded.key == "value")
		#expect(decoded.another == "# This is not a comment")
	}

	struct SimpleContainerType : Codable
	{
		// swiftlint:disable:next nesting
		struct NestedType : Codable
		{
			var count : Int
			var title : String
		}

		var key : String
		var another : String
		var nested : NestedType
	}

	@Test("encoding a nested struct")
	func encodingNestedStruct() throws
	{
		let example = SimpleContainerType(key: "hello", another: "test", nested: .init(count: 5, title: "bird's nest") )

		let expected = """
			key = "hello"
			another = "test"

			[nested]
			count = 5
			title = "bird's nest"
			"""

		let archive = try TOMLikeCoder.encode(example)
		#expect(archive == expected)
	}

	@Test("omitting empty lines")
	func omittingEmptyLines() throws
	{
		struct SomeStruct : Codable
		{
			// swiftlint:disable:next nesting
			struct NestedStruct : Codable
			{
				var a : Int
				var b : Int
			}
			var header : NestedStruct
		}

		let archive = Archive(from: """
			[header]
			a

			b
			"""
		)
		try #require(archive.tables.count == 1)
		#expect(archive.tables[0].lines.count == 2)
		#expect(archive.issues.count == 0)
		_ = try? TOMLikeCoder.decode(SomeStruct.self, from: archive)
		#expect(archive.issues.count == 2)
		#expect(archive.issues.issues.filter{ $0.code == .invalidExpression }.count == 2)
	}

	@Test("decoding dotted key-value pairs")
	func decodingDottedKeyValuePairs() throws
	{
		struct SomeStruct : Codable
		{
			// swiftlint:disable:next nesting
			struct NestedStruct : Codable
			{
				var a : Int
				var b : String
			}
			var namespace : NestedStruct
		}

		let archive = Archive(from: """
			namespace.a = 2
			namespace.b = "bla"
			""")
		try #require(archive.tables.count == 1)
		try #require(archive.tables[0].lines.count == 2)
		let _ = try TOMLikeCoder.decode(SomeStruct.self, from: archive)
		try #require(archive.tables.count == 2)
		#expect(archive.tables[0].path.path == [String]())
		#expect(archive.tables[0].lines.count == 2)
		#expect(archive.tables[0].values.count == 0)
		#expect(archive.tables[1].path.path == ["namespace"])
		#expect(archive.tables[1].lines.count == 0)
		#expect(archive.tables[1].values.count == 2)
		#expect(archive.tables[1]["a"] == "2")
		#expect(archive[TablePath(["namespace", "a"])] == "2")
		#expect(archive[TablePath(["namespace", "b"])] == "\"bla\"")
	}

 }
