// swift-tools-version:6.0

import PackageDescription

let package = Package(
	name: "TOMLike",
	platforms: [.iOS(.v17), .macOS(.v14), .tvOS(.v17), .watchOS(.v10)],
	products: [.library(name: "TOMLike", targets: ["TOMLike"])],
	dependencies: [
		.package(url: "https://gitlab.com/Kai.Oezer/IssueCollection", from: "0.5.1")
	],
	targets: [
		.target(
			name: "TOMLike",
			dependencies: ["IssueCollection"]
		),
		.testTarget(
			name: "TOMLikeTests",
			dependencies: ["TOMLike"])
	]
)
