// Copyright 2022 Kai Oezer

import Foundation

struct TablePath : Equatable, Hashable, CustomStringConvertible
{
	static let separator = "."

	let path : [String]

	init(_ path : [String] = []) { self.path = path }
	init(_ keys : [CodingKey]?) { self.path = keys?.map{ $0.stringValue } ?? [] }
	var length : Int { path.count }
	var isEmpty : Bool { path.isEmpty }
	var parent : TablePath { TablePath([String](path[..<path.index(before:path.endIndex)])) }
	var node : String? { path.last }
	var description : String { path.joined(separator: Self.separator) }
	static func == (lhs : TablePath, rhs : TablePath) -> Bool { lhs.path == rhs.path }
	static func == (lhs : TablePath, rhs : [String]) -> Bool { lhs.path == rhs }
	static func + (lhs : TablePath, rhs : TablePath) -> TablePath { TablePath(lhs.path + rhs.path) }
}

// MARK: -

class Table
{
	static let startMarker = "["
	static let endMarker = "]"

	public var lines = [IndexedLine]()

	public var values = [String : String]()

	/// container hierarchy for the item, empty if top item
	var path = TablePath()

	init(path : TablePath = TablePath(), lines : [IndexedLine] = [])
	{
		self.path = path
		self.lines = lines
	}

	/// - returns: the first available value for the given key if the item contains a line with a matching key-value pair, otherwise ``nil``
	subscript(key : String) -> String?
	{
		values[key]
	}
}

extension Table : CustomStringConvertible
{
	public var description : String
	{
		var descriptionLines = [String]()
		if !path.description.isEmpty { descriptionLines.append( "\(Self.startMarker)\(path)\(Self.endMarker)" ) }
		descriptionLines.append(contentsOf: lines.map{ $0.content })
		return descriptionLines.joined(separator: "\n")
	}
}
