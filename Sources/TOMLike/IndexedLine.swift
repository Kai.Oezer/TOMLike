// Copyright 2022-2024 Kai Oezer

import Foundation

public struct LineNumber : ExpressibleByIntegerLiteral, CustomStringConvertible, Sendable
{
	public let index : Int

	public init(_ lineIndex : Int)
	{
		index = lineIndex
	}

	public init(integerLiteral value: IntegerLiteralType)
	{
		index = value
	}

	public var description : String { String(index) }

	public static let none : LineNumber = -1
}

public struct IndexedLine : Sendable
{
	public var lineNumber : LineNumber
	public var content : String

	public init(_ lineNumber : LineNumber = .none, content : String)
	{
		self.lineNumber = lineNumber
		self.content = content
	}

	public init(_ lineNumber : Int, content : String)
	{
		self.init(LineNumber(lineNumber), content: content)
	}
}
