// Copyright 2022-2024 Kai Oezer

import Foundation
import IssueCollection

extension IssueCode
{
	public static let invalidExpression : IssueCode = 100
	public static let malformedTableHeader : IssueCode = 101
}

extension IssueDomain
{
	public static let TOMLike : IssueDomain = "TOMLike"
}

extension IssueMetadataKey
{
	public static let lineNumber : IssueMetadataKey = "TOMLike.line_number"
}

func tomlikeIssue(_ code : IssueCode, line : LineNumber = .none, message : String? = nil) -> Issue
{
	var metadata : Issue.Metadata = [.lineNumber : String(line.index)]
	if let message {
		metadata[.message] = message
	}
	return Issue(domain: .TOMLike, code: code, metadata: metadata)
}
