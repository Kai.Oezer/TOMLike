// Copyright 2022-2024 Kai Oezer

import Foundation
import RegexBuilder

/// Defines ICU-compatible regular expressions used by the decoder.
///
/// See https://unicode-org.github.io/icu/userguide/strings/regexp.html
struct Pattern
{
	/// An entity name is the base name for a value's identifier or a ``TablePath``.
	static var entityName : Regex<Substring> {
		/[a-zA-Z_][a-zA-Z0-9\-+_*]*/
	}

	/// A sequence of entity names
	/// An entity path is the fully qualified identifier for the entity.
	///
	/// A sequence of ``entityName``, where ``pathSeparator`` is used to join the entity names.
	static var entityPath : Regex<Regex<Substring>.RegexOutput> {
		Regex {
			entityName
			ZeroOrMore
			{
				TablePath.separator
				entityName
			}
		}
	}

	static var tablePath : Regex<Regex<Substring>.RegexOutput> {
		Regex {
			entityName
			ZeroOrMore
			{
				ZeroOrMore{ .whitespace }
				TablePath.separator
				ZeroOrMore{ .whitespace }
				entityName
			}
		}
	}

// MARK: - Comment

	/// A trailing comment pattern where the content is captured in the first group.
	static var comment : Regex<(Substring,Substring)> {
		/#(.*)$/
	}

	static var commentOnlyLine : Regex<Regex<(Substring, Regex<ZeroOrMore<Substring>.RegexOutput>.RegexOutput, Substring)>.RegexOutput> {
		Regex {
			Anchor.startOfLine
			Capture {
				ZeroOrMore{ .whitespace }
			}
			comment
		}
	}

	/// table header without trailing comment
	///
	/// example: `[person.name]`
	static var tableHeader : Regex<Regex<(Substring, Regex<Regex<Regex<Substring>.RegexOutput>.RegexOutput>.RegexOutput)>.RegexOutput> {
		Regex {
			Anchor.startOfLine
			ZeroOrMore{ .whitespace }
			"["
			ZeroOrMore{ .whitespace }
			Capture { tablePath }
			ZeroOrMore{ .whitespace }
			"]"
			ZeroOrMore{ .whitespace }
			Anchor.endOfLine
		}
	}

// MARK: - Key-Value Pairs

	static var keyValuePair : Regex<Regex<(Substring, Regex<Regex<Regex<Substring>.RegexOutput>.RegexOutput>.RegexOutput, Regex<OneOrMore<Substring>.RegexOutput>.RegexOutput)>.RegexOutput> {
		Regex {
			_keyValuePairStart
			Capture
			{
				OneOrMore{ .any }
			}
			ZeroOrMore{ .whitespace }
			Anchor.endOfLine
		}
	}

	/// The pattern for a key-value pair, whose value is a string, with trailing comment.
	///
	/// The first capture group captures the key-value pair line without the trailing comment.
	static var keyValuePairWithStringValue : Regex<Regex<(Substring, Regex<Regex<Regex<Substring>.RegexOutput>.RegexOutput>.RegexOutput, Substring, Substring, Substring?)>.RegexOutput> {
		Regex {
			_keyValuePairStart
			stringWithEscapes
			ZeroOrMore{ .whitespace }
			ChoiceOf {
				comment
				Anchor.endOfLine
			}
		}
	}

	private static var _keyValuePairStart : Regex<Regex<(Substring, Regex<Regex<Regex<Substring>.RegexOutput>.RegexOutput>.RegexOutput)>.RegexOutput> {
		Regex {
			Anchor.startOfLine
			Capture { entityPath }
			ZeroOrMore{ .whitespace }
			"="
			ZeroOrMore{ .whitespace }
		}
	}

// MARK: - Strings

	/// pattern for a string containing optional escaped characters, where the quote symbol can be either `"` or `'`
	static var stringWithEscapes : Regex<(Substring, Substring, Substring)> {
		/(['"])((?:(?!(?:\\|\1)).|\\.)*)\1/
	}

	static var snippetWithString : Regex<Regex<(Substring, Substring, Substring)>.RegexOutput> {
		Regex {
			/[^#'"]*/
			stringWithEscapes
			/[^#'"]*/
		}
	}

	static var lineWithString : Regex<Regex<(Substring, Substring, Substring)>.RegexOutput> {
		Regex {
			Anchor.startOfLine
			snippetWithString
		}
	}

	static var textLine : Regex<Regex<(Substring, Substring, Substring)>.RegexOutput> {
		Regex {
			Anchor.startOfLine
			ZeroOrMore{ .whitespace }
			Self.stringWithEscapes
			ZeroOrMore{ .whitespace }
			Anchor.endOfLine
		}
	}

	static var lineWithoutStringWithComment : Regex<Regex<(Substring, Regex<Regex<Substring>.RegexOutput>.RegexOutput, Substring)>.RegexOutput> {
		Regex {
			Anchor.startOfLine
			Capture {
				/[^#"']*/
			}
			comment
		}
	}

// MARK: - Numbers

	static var value_integer : Regex<Substring> {
		/[-+]?[0-9]*/
	}

	static var value_float : Regex<Substring> {
		/[-+]?[0-9]*(?:\.[0-9]*)?/
	}
}
