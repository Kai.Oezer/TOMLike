//  Copyright 2022-2024 Kai Oezer

import Foundation
import IssueCollection

class Archive
{
	var issues = IssueCollection()
	var tables = [Table]()

	init(lines : [IndexedLine] = [])
	{
		extractTables(from: lines)
	}

	convenience init(from stringRepresentation : String)
	{
		var lines = [IndexedLine]()
		var lineIndex = 0
		stringRepresentation.enumerateLines { (line, _) in
			let line = line.strippingTOMLikeComments
			if !line.isEmpty
			{
				lines.append(.init(lineIndex, content: line))
			}
			lineIndex += 1
		}
		self.init(lines: lines)
	}

	func extractTables(from lines : [IndexedLine])
	{
		var currentTable : Table?

		for line in lines
		{
			let lineContent = line.content
			guard let (_, path) = lineContent.wholeMatch(of: Pattern.tableHeader)?.output else {
				if currentTable == nil
				{
					currentTable = findOrCreateTable(for: TablePath())
				}
				currentTable?.lines.append(line)
				continue
			}
			let pathComponents = path.components(separatedBy: TablePath.separator).map{ $0.trimmingCharacters(in: .whitespaces) }
			currentTable = findOrCreateTable(for: TablePath(pathComponents))
		}
	}

	/// Scans the lines of the given table for key-value pairs and inserts the values into the target table determined from the key.
	///
	/// This method should be called only after all tables have been extracted from the input lines.
	/// Key-value pairs defined in the input lines may have keys with path prefixes, which causes the values to be assigned to a child table.
	func extractValuesFromLines(in baseTable : Table)
	{
		for line in baseTable.lines
		{
			let lineContent = line.content
			guard let (_, key, value) = lineContent.wholeMatch(of: Pattern.keyValuePair)?.output else {
				issues.append(tomlikeIssue(.invalidExpression, line: line.lineNumber, message: "Invalid expression."))
				continue
			}

			let keyPath = TablePath( key.components(separatedBy: TablePath.separator).map{ $0.trimmingCharacters(in: .whitespaces) })
			let keyAbsolutePath = baseTable.path + keyPath
			guard let key = keyAbsolutePath.node else {
				issues.append(tomlikeIssue(.invalidExpression, line: line.lineNumber, message: "Invalid expression."))
				continue
			}

			let tablePath = keyAbsolutePath.parent
			let table = findOrCreateTable(for: tablePath)
			table.values[key] = value.trimmingCharacters(in: .whitespaces)
		}
	}

	/// - returns: the table at the given path if it exists, otherwise ``nil``
	func findTable(for path : TablePath) -> Table?
	{
		self.tables.first{ $0.path == path }
	}

	/// - returns: the table at the given path
	func findOrCreateTable(for path : TablePath) -> Table
	{
		if let existingTable = self.tables.first(where:{ $0.path == path }) {
			return existingTable
		}
		let newTable = Table(path: path)
		self.tables.append(newTable)
		return newTable
	}

	func table(at path : TablePath) -> Table?
	{
		tables.first{ $0.path == path }
	}

	func table(at path : [CodingKey]) -> Table?
	{
		table(at: TablePath(path))
	}

	func value(at path : TablePath) -> String?
	{
		guard tables.first(where:{ $0.path == path }) == nil else { return nil }
		let parentPath = path.parent
		guard let table = tables.first(where:{ $0.path == parentPath }),
			let key = path.path.last
			else { return nil }
		extractValuesFromLines(in: table)
		return table[key]
	}

	func value(at path : [CodingKey]) -> String?
	{
		value(at: TablePath(path))
	}

	subscript (path : TablePath) -> String?
	{
		value(at: path)
	}

	func encode(embedded : String, for path : [CodingKey]?)
	{
		tables.append(Table(path: TablePath(path), lines: [.init(.none, content: embedded)]))
	}

	func encode(key : String, value : String, for path : [CodingKey]?)
	{
		let tablePath = TablePath(path)
		let newLine = IndexedLine(content: "\(key) = \(value)")
		if let table = self.table(at: tablePath)
		{
			table.lines.append(newLine)
		}
		else
		{
			tables.append(Table(path: tablePath, lines: [newLine]))
		}
	}

	func encode(value : String, for path : [CodingKey]?)
	{
		let tablePath = TablePath(path)
		let newLine = IndexedLine(content: value)
		if let table = self.table(at: tablePath)
		{
			table.lines.append(newLine)
		}
		else
		{
			tables.append(Table(path: tablePath, lines: [newLine]))
		}
	}

	func tableLines(for path : TablePath) -> [IndexedLine]?
	{
		self.table(at: path)?.lines
	}

	func containsItemOrValue(for keyPath : [CodingKey]) -> Bool
	{
		let tablePath = TablePath(keyPath)
		let table = table(at: tablePath)
		if table != nil { return true }
		return value(at: tablePath) != nil
	}
}

extension Archive : CustomStringConvertible
{
	var description: String
	{
		tables.map{$0.description}.joined(separator:"\n\n")
	}
}

extension String
{
	/// Strips a comment (starting with '#') from the end of the line.
	///
	/// The implementation looks for the first occurrence of '#' not wrapped in quotes.
	var strippingTOMLikeComments : String
	{
		var remainingSubstringAfterSkippingQuotedParts : Substring = self[self.startIndex...]
		while true {
			guard let range = remainingSubstringAfterSkippingQuotedParts.firstMatch(of: Pattern.snippetWithString)?.range else { break }
			remainingSubstringAfterSkippingQuotedParts = remainingSubstringAfterSkippingQuotedParts[range.upperBound...]
		}
		if let commentRange = remainingSubstringAfterSkippingQuotedParts.firstMatch(of: Pattern.comment)?.range {
			return String(self[..<commentRange.lowerBound])
		}

		return self
	}
}
