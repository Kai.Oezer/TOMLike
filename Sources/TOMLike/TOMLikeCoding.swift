// Copyright 2022-2024 Kai Oezer

import Foundation
import IssueCollection

public struct TOMLikeCoder
{
	public static func encode(_ value : Encodable) throws -> String
	{
		if let encodableValue = value as? TOMLikeEmbeddable
		{
			return try encodableValue.tomlikeEmbedded()
		}
		else
		{
			let encoder = TOMLikeEncoder()
			try value.encode(to: encoder)
			return encoder.archive.description
		}
	}

	public static func decode<T>(_ type : T.Type, from serializedArchive : String, issues : inout IssueCollection) throws -> T where T : Decodable
	{
		let archive = Archive(from: serializedArchive)
		let decoded = try decode(type, from: archive)
		issues = archive.issues
		return decoded
	}

	static func decode<T>(_ type : T.Type, from archive : Archive) throws -> T where T : Decodable
	{
		if archive.tables.count == 1, let decodableType = type as? TOMLikeEmbeddable.Type
		{
			// swiftlint:disable:next force_cast
			return try decodableType.init(tomlikeEmbedded: archive.tables[0].lines, issues: &archive.issues) as! T
		}
		else
		{
			return try type.init(from: TOMLikeDecoder(archive: archive, codingPath: []))
		}
	}
}

extension TOMLikeCoder
{
	public static func copy(_ input : String, excludingPrefix prefix : String) -> String
	{
		String(input[input.index(input.startIndex, offsetBy:prefix.count)...])
	}
}

extension DecodingError
{
	static func tomlikeSyntaxError(in value : String, at codingPath : [CodingKey]) -> DecodingError
	{
		.dataCorrupted(.init(codingPath: codingPath, debugDescription: "Syntax error in \"\(value)\".", underlyingError: nil))
	}

	static func tomlikeValueNotFound(for type : Any.Type, at codingPath : [CodingKey]) -> DecodingError
	{
		.valueNotFound(type, .init(codingPath: codingPath, debugDescription: "No value found for type \"\(type)\".", underlyingError: nil))
	}
}

extension Date
{
	static var tomlFormatter : ISO8601DateFormatter {
		let formatter = ISO8601DateFormatter()
		formatter.formatOptions = [.withFullDate, .withFullTime, .withDashSeparatorInDate]
		return formatter
	}
}

extension String
{
	init?(tomlikeEncoded encoded : String)
	{
		guard let (_, _, content) = encoded.wholeMatch(of: Pattern.textLine)?.output else { return nil }
		self = String(content)
	}
}
