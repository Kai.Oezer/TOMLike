// Copyright 2022-2024 Kai Oezer
// swiftlint:disable file_length

import Foundation
import IssueCollection

struct TOMLikeDecoder : Decoder
{
	let archive : Archive
	var codingPath: [CodingKey]

	var userInfo: [CodingUserInfoKey : Any] = [:]

	func container<Key>(keyedBy type: Key.Type) throws -> KeyedDecodingContainer<Key> where Key : CodingKey
	{
		KeyedDecodingContainer(TOMLikeKeyedDecoder<Key>(archive: archive, codingPath: codingPath))
	}

	func unkeyedContainer() throws -> UnkeyedDecodingContainer
	{
		TOMLikeUnkeyedDecoder(archive : archive, codingPath: codingPath)
	}

	func singleValueContainer() throws -> SingleValueDecodingContainer
	{
		TOMLikeSingleValueDecoder(archive : archive, codingPath: codingPath)
	}

	static func decodeOtherTOMLValue<T>(_ value : String) -> T? where T : Decodable
	{
		if T.self == Date.self
		{
			if let date = Date.tomlFormatter.date(from: value)
			{
				return date as? T
			}
		}

		return nil
	}

}

// MARK: -

private struct TOMLikeKeyedDecoder<Key> : KeyedDecodingContainerProtocol where Key : CodingKey
{
	let archive : Archive
	var codingPath: [CodingKey]

	private(set) var allKeys: [Key] = []

	func contains(_ key: Key) -> Bool
	{
		archive.containsItemOrValue(for: codingPath + [key])
	}

	func decodeNil(forKey key: Key) throws -> Bool
	{
		false
	}

	func decode(_ type: Bool.Type, forKey key: Key) throws -> Bool
	{
		let stringValue = try _extractStringValue(for: Bool.self, key: key)
		guard let result = Bool(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [key])
		}
		return result
	}

	func decode(_ type: String.Type, forKey key: Key) throws -> String
	{
		let quotedValue = try _extractStringValue(for: String.self, key: key)
		guard let result = String(tomlikeEncoded: quotedValue) else {
			throw DecodingError.tomlikeSyntaxError(in: quotedValue, at: codingPath + [key])
		}
		return result
	}

	func decode(_ type: Double.Type, forKey key: Key) throws -> Double
	{
		let stringValue = try _extractStringValue(for: Double.self, key: key)
		guard let result = Double(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [key])
		}
		return result
	}

	func decode(_ type: Float.Type, forKey key: Key) throws -> Float
	{
		let stringValue = try _extractStringValue(for: Float.self, key: key)
		guard let result = Float(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [key])
		}
		return result
	}

	func decode(_ type: Int.Type, forKey key: Key) throws -> Int
	{
		let stringValue = try _extractStringValue(for: Int.self, key: key)
		guard let result = Int(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [key])
		}
		return result
	}

	func decode(_ type: Int8.Type, forKey key: Key) throws -> Int8
	{
		let stringValue = try _extractStringValue(for: Int8.self, key: key)
		guard let result = Int8(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [key])
		}
		return result
	}

	func decode(_ type: Int16.Type, forKey key: Key) throws -> Int16
	{
		let stringValue = try _extractStringValue(for: Bool.self, key: key)
		guard let result = Int16(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [key])
		}
		return result
	}

	func decode(_ type: Int32.Type, forKey key: Key) throws -> Int32
	{
		let stringValue = try _extractStringValue(for: Int32.self, key: key)
		guard let result = Int32(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [key])
		}
		return result
	}

	func decode(_ type: Int64.Type, forKey key: Key) throws -> Int64
	{
		let stringValue = try _extractStringValue(for: Int64.self, key: key)
		guard let result = Int64(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [key])
		}
		return result
	}

	func decode(_ type: UInt.Type, forKey key: Key) throws -> UInt
	{
		let stringValue = try _extractStringValue(for: UInt.self, key: key)
		guard let result = UInt(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [key])
		}
		return result
	}

	func decode(_ type: UInt8.Type, forKey key: Key) throws -> UInt8
	{
		let stringValue = try _extractStringValue(for: UInt8.self, key: key)
		guard let result = UInt8(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [key])
		}
		return result
	}

	func decode(_ type: UInt16.Type, forKey key: Key) throws -> UInt16
	{
		let stringValue = try _extractStringValue(for: UInt16.self, key: key)
		guard let result = UInt16(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [key])
		}
		return result
	}

	func decode(_ type: UInt32.Type, forKey key: Key) throws -> UInt32
	{
		let stringValue = try _extractStringValue(for: UInt32.self, key: key)
		guard let result = UInt32(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [key])
		}
		return result
	}

	func decode(_ type: UInt64.Type, forKey key: Key) throws -> UInt64
	{
		let stringValue = try _extractStringValue(for: UInt64.self, key: key)
		guard let result = UInt64(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [key])
		}
		return result
	}

	private func _extractStringValue(for type : Any.Type, key : Key) throws -> String
	{
		guard let value = archive.value(at: codingPath + [key]) else {
			throw DecodingError.tomlikeValueNotFound(for: type, at: codingPath + [key])
		}
		return value
	}

	func decode<T>(_ type: T.Type, forKey key: Key) throws -> T where T : Decodable
	{
		if let embeddableType = type as? TOMLikeEmbeddable.Type
		{
			let path = codingPath + [key]
			guard let archivedContent = archive.tableLines(for: TablePath(path)) else {
				throw DecodingError.tomlikeValueNotFound(for: type, at: codingPath + [key])
			}
			// swiftlint:disable:next force_cast
			return try embeddableType.init(tomlikeEmbedded: archivedContent, issues: &archive.issues) as! T
		}

		if let table = archive.findTable(for: TablePath(codingPath))
		{
			archive.extractValuesFromLines(in: table)
		}

		if let value = try? _extractStringValue(for: type, key: key),
			let decoded : T = TOMLikeDecoder.decodeOtherTOMLValue(value)
		{
			return decoded
		}

		let decoder = TOMLikeDecoder(archive: archive, codingPath: codingPath + [key])
		let decoded = try type.init(from: decoder)
		return decoded
	}

	func decodeIfPresent<T>(_ type: T.Type, forKey key: Key) throws -> T? where T : Decodable
	{
		if let embeddableType = type as? TOMLikeEmbeddable.Type
		{
			let path = codingPath + [key]
			guard let archivedContent = archive.tableLines(for: TablePath(path)) else { return nil }
			return try embeddableType.init(tomlikeEmbedded: archivedContent, issues: &archive.issues) as? T
		}

		if let table = archive.findTable(for: TablePath(codingPath))
		{
			archive.extractValuesFromLines(in: table)
		}

		if let value = try? _extractStringValue(for: type, key: key),
			let decoded : T = TOMLikeDecoder.decodeOtherTOMLValue(value)
		{
			return decoded
		}

		return try? type.init(from: TOMLikeDecoder(archive: archive, codingPath: codingPath + [key]))
	}

	func nestedContainer<NestedKey>(keyedBy type: NestedKey.Type, forKey key: Key) throws -> KeyedDecodingContainer<NestedKey> where NestedKey : CodingKey
	{
		KeyedDecodingContainer(TOMLikeKeyedDecoder<NestedKey>(archive: archive, codingPath: codingPath + [key]))
	}

	func nestedUnkeyedContainer(forKey key: Key) throws -> UnkeyedDecodingContainer
	{
		TOMLikeUnkeyedDecoder(archive: archive, codingPath: codingPath + [key])
	}

	func superDecoder() throws -> Decoder
	{
		try superDecoder(forKey: Key(stringValue: "super")!)
	}

	func superDecoder(forKey key: Key) throws -> Decoder
	{
		TOMLikeDecoder(archive: archive, codingPath: codingPath + [key])
	}
}

// MARK: -

private struct TOMLikeUnkeyedDecoder : UnkeyedDecodingContainer
{
	struct IndexedCodingKey : CodingKey
	{
		let intValue : Int?
		let stringValue : String
		init?(intValue : Int) { self.intValue = intValue; self.stringValue = intValue.description }
		init?(stringValue : String) { return nil }
	}

	private mutating func nextIndexedKey() throws -> CodingKey
	{
		let key = IndexedCodingKey(intValue: self.currentIndex)!
		currentIndex += 1
		return key
	}

	let archive : Archive
	var codingPath : [CodingKey]

	var count : Int? { archive.table(at: codingPath)?.lines.count ?? 0 }

	var isAtEnd : Bool { currentIndex >= (count ?? 0) }

	private(set) var currentIndex : Int = 0

	func decodeNil() throws -> Bool
	{
		false
	}

	func decode(_ type: Bool.Type) throws -> Bool
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = Bool(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	func decode(_ type: String.Type) throws -> String
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = String(tomlikeEncoded: stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	func decode(_ type: Double.Type) throws -> Double
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = Double(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	func decode(_ type: Float.Type) throws -> Float
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = Float(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	func decode(_ type: Int.Type) throws -> Int
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = Int(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	func decode(_ type: Int8.Type) throws -> Int8
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = Int8(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	func decode(_ type: Int16.Type) throws -> Int16
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = Int16(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	func decode(_ type: Int32.Type) throws -> Int32
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = Int32(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	func decode(_ type: Int64.Type) throws -> Int64
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = Int64(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	func decode(_ type: UInt.Type) throws -> UInt
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = UInt(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	func decode(_ type: UInt8.Type) throws -> UInt8
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = UInt8(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	func decode(_ type: UInt16.Type) throws -> UInt16
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = UInt16(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	func decode(_ type: UInt32.Type) throws -> UInt32
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = UInt32(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	func decode(_ type: UInt64.Type) throws -> UInt64
	{
		let stringValue = try _extractStringValue(for: type)
		guard let value = UInt64(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])
		}
		return value
	}

	private func _extractStringValue(for type : Any.Type) throws -> String
		{
			guard let table = archive.table(at: codingPath),
				table.lines.count > self.currentIndex
				else { throw DecodingError.tomlikeValueNotFound(for: type, at: codingPath + [IndexedCodingKey(intValue: self.currentIndex)!])}
		return table.lines[self.currentIndex].content
	}

	mutating func decode<T>(_ type: T.Type) throws -> T where T : Decodable
	{
		if let embeddableType = type as? TOMLikeEmbeddable.Type
		{
			guard let lines = archive.tableLines(for: TablePath(codingPath)) else {
				throw DecodingError.tomlikeValueNotFound(for: type, at: codingPath)
			}
			// swiftlint:disable:next force_cast
			return try embeddableType.init(tomlikeEmbedded: lines, issues: &archive.issues) as! T
		}
		else if let lines = archive.tableLines(for: TablePath(codingPath)),
			lines.count > currentIndex,
			let decoded : T = TOMLikeDecoder.decodeOtherTOMLValue(lines[currentIndex].content)
		{
			return decoded
		}
		else
		{
			return try type.init(from: TOMLikeDecoder(archive: archive, codingPath: codingPath + [nextIndexedKey()]))
		}
	}

	mutating func decodeIfPresent<T>(_ type: T.Type) throws -> T? where T : Decodable
	{
		if let embeddableType = type as? TOMLikeEmbeddable.Type
		{
			guard let lines = archive.tableLines(for: TablePath(codingPath)) else { return nil }
			return try embeddableType.init(tomlikeEmbedded: lines, issues: &archive.issues) as? T
		}
		else if let lines = archive.tableLines(for: TablePath(codingPath)),
			lines.count > currentIndex,
			let decoded : T = TOMLikeDecoder.decodeOtherTOMLValue(lines[currentIndex].content)
		{
			return decoded
		}
		else
		{
			return try type.init(from: TOMLikeDecoder(archive: archive, codingPath: codingPath + [nextIndexedKey()]))
		}
	}

	mutating func nestedContainer<NestedKey>(keyedBy type: NestedKey.Type) throws -> KeyedDecodingContainer<NestedKey> where NestedKey : CodingKey
	{
		try KeyedDecodingContainer(TOMLikeKeyedDecoder<NestedKey>(archive: archive, codingPath: codingPath + [nextIndexedKey()]))
	}

	mutating func nestedUnkeyedContainer() throws -> UnkeyedDecodingContainer
	{
		try TOMLikeUnkeyedDecoder(archive: archive, codingPath: codingPath + [nextIndexedKey()])
	}

	mutating func superDecoder() throws -> Decoder
	{
		try TOMLikeDecoder(archive: archive, codingPath: codingPath + [nextIndexedKey()])
	}
}

// MARK: -

private struct TOMLikeSingleValueDecoder : SingleValueDecodingContainer
{
	let archive : Archive
	var codingPath : [CodingKey]

	func decodeNil() -> Bool { false }

	func decode(_ type: Bool.Type) throws -> Bool
	{
		let stringValue = try _extractValue(for: type)
		guard let result = Bool(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath)
		}
		return result
	}

	func decode(_ type: String.Type) throws -> String
	{
		let encodedString = try _extractValue(for: type)
		guard let result = String(tomlikeEncoded: encodedString) else {
			throw DecodingError.tomlikeSyntaxError(in: encodedString, at: codingPath)
		}
		return result
	}

	func decode(_ type: Double.Type) throws -> Double
	{
		let stringValue = try _extractValue(for: type)
		guard let result = Double(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath)
		}
		return result
	}

	func decode(_ type: Float.Type) throws -> Float
	{
		let stringValue = try _extractValue(for: type)
		guard let result = Float(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath)
		}
		return result
	}

	func decode(_ type: Int.Type) throws -> Int
	{
		let stringValue = try _extractValue(for: type)
		guard let result = Int(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath)
		}
		return result
	}

	func decode(_ type: Int8.Type) throws -> Int8
	{
		let stringValue = try _extractValue(for: type)
		guard let result = Int8(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath)
		}
		return result
	}

	func decode(_ type: Int16.Type) throws -> Int16
	{
		let stringValue = try _extractValue(for: type)
		guard let result = Int16(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath)
		}
		return result
	}

	func decode(_ type: Int32.Type) throws -> Int32
	{
		let stringValue = try _extractValue(for: type)
		guard let result = Int32(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath)
		}
		return result
	}

	func decode(_ type: Int64.Type) throws -> Int64
	{
		let stringValue = try _extractValue(for: type)
		guard let result = Int64(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath)
		}
		return result
	}

	func decode(_ type: UInt.Type) throws -> UInt
	{
		let stringValue = try _extractValue(for: type)
		guard let result = UInt(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath)
		}
		return result
	}

	func decode(_ type: UInt8.Type) throws -> UInt8
	{
		let stringValue = try _extractValue(for: type)
		guard let result = UInt8(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath)
		}
		return result
	}

	func decode(_ type: UInt16.Type) throws -> UInt16
	{
		let stringValue = try _extractValue(for: type)
		guard let result = UInt16(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath)
		}
		return result
	}

	func decode(_ type: UInt32.Type) throws -> UInt32
	{
		let stringValue = try _extractValue(for: type)
		guard let result = UInt32(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath)
		}
		return result
	}

	func decode(_ type: UInt64.Type) throws -> UInt64
	{
		let stringValue = try _extractValue(for: type)
		guard let result = UInt64(stringValue) else {
			throw DecodingError.tomlikeSyntaxError(in: stringValue, at: codingPath)
		}
		return result
	}

	func decode<T>(_ type: T.Type) throws -> T where T : Decodable
	{
		let path = TablePath(codingPath)
		guard let archivedContent = archive.tableLines(for: path) else {
			throw DecodingError.tomlikeValueNotFound(for: type, at: codingPath)
		}
		if let embeddableType = type as? TOMLikeEmbeddable.Type
		{
			// swiftlint:disable:next force_cast
			return try embeddableType.init(tomlikeEmbedded: archivedContent, issues: &archive.issues) as! T
		}
		else if archivedContent.count == 1,
			let line = archivedContent.first,
			let decoded : T = TOMLikeDecoder.decodeOtherTOMLValue(line.content)
		{
			return decoded
		}
		else
		{
			return try type.init(from: TOMLikeDecoder(archive: archive, codingPath: codingPath))
		}
	}

	private func _extractValue<T>(for type : T.Type) throws -> String
	{
		guard let value = archive.value(at: codingPath) else {
			throw DecodingError.tomlikeValueNotFound(for: type, at: [])
		}
		return value
	}
}
