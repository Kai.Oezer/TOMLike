// Copyright 2022-2023 Kai Oezer

import Foundation

/**
	TOMLikeEncoder collects the Archive representations of BHEncodable elements within
	an Encodable data structure, ignoring elements that are not BHEncodable-compliant.
*/
struct TOMLikeEncoder : Encoder
{
	let archive : Archive

	init(archive : Archive = Archive())
	{
		self.archive = archive
		self.codingPath = []
	}

	var codingPath : [CodingKey]

	var userInfo : [CodingUserInfoKey : Any] = [:]

	func container<Key: CodingKey>(keyedBy type: Key.Type) -> KeyedEncodingContainer<Key>
	{
		var container = TOMLikeKeyedEncoder<Key>(to: archive)
		container.codingPath = codingPath
		return KeyedEncodingContainer(container)
	}

	func unkeyedContainer() -> UnkeyedEncodingContainer
	{
		var container = TOMLikeMultiEncoder(to: archive)
		container.codingPath = codingPath
		return container
	}

	func singleValueContainer() -> SingleValueEncodingContainer
	{
		var container = TOMLikeSingleValueEncoder(to: archive)
		container.codingPath = codingPath
		return container
	}

	static func encodeOtherTOMLValue<T>(_ value : T) -> String? where T : Encodable
	{
		if let date = value as? Date
		{
			return Date.tomlFormatter.string(from: date)
		}

		return nil
	}
}

private struct TOMLikeKeyedEncoder<Key : CodingKey> : KeyedEncodingContainerProtocol
{
	private var archive : Archive

	init(to archive : Archive)
	{
		self.archive = archive
	}

	var codingPath: [CodingKey] = []

	// swiftlint:disable comma
	mutating func encodeNil(forKey key: Key)               throws { /* ignore */ }
	mutating func encode(_ value: Bool,   forKey key: Key) throws {archive.encode(key: key.stringValue, value: value ? "true" : "false", for: codingPath)}
	mutating func encode(_ value: String, forKey key: Key) throws {archive.encode(key: key.stringValue, value: "\"\(value)\"", for: codingPath)}
	mutating func encode(_ value: Double, forKey key: Key) throws {archive.encode(key: key.stringValue, value: "\(value)", for: codingPath)}
	mutating func encode(_ value: Float,  forKey key: Key) throws {archive.encode(key: key.stringValue, value: "\(value)", for: codingPath)}
	mutating func encode(_ value: Int,    forKey key: Key) throws {archive.encode(key: key.stringValue, value: "\(value)", for: codingPath)}
	mutating func encode(_ value: Int8,   forKey key: Key) throws {archive.encode(key: key.stringValue, value: "\(value)", for: codingPath)}
	mutating func encode(_ value: Int16,  forKey key: Key) throws {archive.encode(key: key.stringValue, value: "\(value)", for: codingPath)}
	mutating func encode(_ value: Int32,  forKey key: Key) throws {archive.encode(key: key.stringValue, value: "\(value)", for: codingPath)}
	mutating func encode(_ value: Int64,  forKey key: Key) throws {archive.encode(key: key.stringValue, value: "\(value)", for: codingPath)}
	mutating func encode(_ value: UInt,   forKey key: Key) throws {archive.encode(key: key.stringValue, value: "\(value)", for: codingPath)}
	mutating func encode(_ value: UInt8,  forKey key: Key) throws {archive.encode(key: key.stringValue, value: "\(value)", for: codingPath)}
	mutating func encode(_ value: UInt16, forKey key: Key) throws {archive.encode(key: key.stringValue, value: "\(value)", for: codingPath)}
	mutating func encode(_ value: UInt32, forKey key: Key) throws {archive.encode(key: key.stringValue, value: "\(value)", for: codingPath)}
	mutating func encode(_ value: UInt64, forKey key: Key) throws {archive.encode(key: key.stringValue, value: "\(value)", for: codingPath)}
	// swiftlint:enable comma

	mutating func encode<T>(_ value: T, forKey key: Key) throws where T : Encodable
	{
		try _encode(value, forKey: key)
	}

	mutating func encodeIfPresent<T>(_ value: T?, forKey key: Key) throws where T : Encodable
	{
		if let value {
			try _encode(value, forKey: key)
		}
	}

	private mutating func _encode<T>(_ value: T, forKey key: Key) throws where T : Encodable
	{
		if let embeddableValue = value as? TOMLikeEmbeddable
		{
			try archive.encode(embedded: embeddableValue.tomlikeEmbedded(), for: codingPath + [key])
		}
		else if let tomlValueRepresentation = TOMLikeEncoder.encodeOtherTOMLValue(value)
		{
			archive.encode(key: key.stringValue, value: tomlValueRepresentation, for: codingPath)
		}
		else
		{
			var encoder = TOMLikeEncoder(archive: archive)
			encoder.codingPath = codingPath + [key]
			try value.encode(to: encoder)
		}
	}

	mutating func nestedContainer<NestedKey>(keyedBy keyType: NestedKey.Type, forKey key: Key) -> KeyedEncodingContainer<NestedKey> where NestedKey : CodingKey
	{
		var container = TOMLikeKeyedEncoder<NestedKey>(to: archive)
		container.codingPath = codingPath + [key]
		return KeyedEncodingContainer(container)
	}

	mutating func nestedUnkeyedContainer(forKey key: Key) -> UnkeyedEncodingContainer
	{
		var container = TOMLikeMultiEncoder(to: archive)
		container.codingPath = codingPath + [key]
		return container
	}

	mutating func superEncoder() -> Encoder
	{
		superEncoder(forKey: Key(stringValue: "super")!)
	}

	mutating func superEncoder(forKey key: Key) -> Encoder
	{
		var encoder = TOMLikeEncoder(archive: archive)
		encoder.codingPath = codingPath + [key]
		return encoder
	}
}

private struct TOMLikeMultiEncoder : UnkeyedEncodingContainer
{
	struct IndexedCodingKey : CodingKey
	{
		let intValue : Int?
		let stringValue : String
		init?(intValue : Int) { self.intValue = intValue; self.stringValue = intValue.description }
		init?(stringValue : String) { return nil }
	}

	private mutating func nextIndexedKey() -> CodingKey
	{
		let key = IndexedCodingKey(intValue: self.count)!
		self.count += 1
		return key
	}

	private var archive : Archive

	init(to archive : Archive)
	{
		self.archive = archive
	}

	var codingPath: [CodingKey] = []

	private(set) var count : Int = 0

	mutating func encodeNil()             throws { /* ignore */ }
	mutating func encode(_ value: Bool)   throws { /* ignore */ }
	mutating func encode(_ value: String) throws { /* ignore */ }
	mutating func encode(_ value: Double) throws { /* ignore */ }
	mutating func encode(_ value: Float)  throws { /* ignore */ }
	mutating func encode(_ value: Int)    throws { /* ignore */ }
	mutating func encode(_ value: Int8)   throws { /* ignore */ }
	mutating func encode(_ value: Int16)  throws { /* ignore */ }
	mutating func encode(_ value: Int32)  throws { /* ignore */ }
	mutating func encode(_ value: Int64)  throws { /* ignore */ }
	mutating func encode(_ value: UInt)   throws { /* ignore */ }
	mutating func encode(_ value: UInt8)  throws { /* ignore */ }
	mutating func encode(_ value: UInt16) throws { /* ignore */ }
	mutating func encode(_ value: UInt32) throws { /* ignore */ }
	mutating func encode(_ value: UInt64) throws { /* ignore */ }

	mutating func encode<T>(_ value: T) throws where T : Encodable
	{
		if let encodableValue = value as? TOMLikeEmbeddable
		{
			try archive.encode(embedded: encodableValue.tomlikeEmbedded(), for: codingPath + [nextIndexedKey()])
		}
		else if let tomlValueRepresentation = TOMLikeEncoder.encodeOtherTOMLValue(value)
		{
			archive.encode(value: tomlValueRepresentation, for: codingPath + [nextIndexedKey()])
		}
		else
		{
			var encoder = TOMLikeEncoder(archive: archive)
			encoder.codingPath = codingPath + [nextIndexedKey()]
			try value.encode(to: encoder)
		}
	}

	mutating func nestedContainer<NestedKey>(keyedBy keyType: NestedKey.Type) -> KeyedEncodingContainer<NestedKey> where NestedKey : CodingKey
	{
		var container = TOMLikeKeyedEncoder<NestedKey>(to: archive)
		container.codingPath = codingPath + [nextIndexedKey()]
		return KeyedEncodingContainer(container)
	}

	mutating func nestedUnkeyedContainer() -> UnkeyedEncodingContainer
	{
		var container = TOMLikeMultiEncoder(to: archive)
		container.codingPath = codingPath + [nextIndexedKey()]
		return container
	}

	mutating func superEncoder() -> Encoder
	{
		var encoder = TOMLikeEncoder(archive: archive)
		encoder.codingPath.append(nextIndexedKey())
		return encoder
	}
}

// MARK: -

/// Although there is no single-value encoding in TOML, it needs to be supported for TOMLikeEmbeddable types.
struct TOMLikeSingleValueEncoder : SingleValueEncodingContainer
{
	var codingPath: [CodingKey] = []

	private var archive : Archive

	init(to archive : Archive)
	{
		self.archive = archive
	}

	mutating func encodeNil()             throws { /* ignore */ }
	mutating func encode(_ value: Bool)   throws { /* ignore */ }
	mutating func encode(_ value: String) throws { /* ignore */ }
	mutating func encode(_ value: Double) throws { /* ignore */ }
	mutating func encode(_ value: Float)  throws { /* ignore */ }
	mutating func encode(_ value: Int)    throws { /* ignore */ }
	mutating func encode(_ value: Int8)   throws { /* ignore */ }
	mutating func encode(_ value: Int16)  throws { /* ignore */ }
	mutating func encode(_ value: Int32)  throws { /* ignore */ }
	mutating func encode(_ value: Int64)  throws { /* ignore */ }
	mutating func encode(_ value: UInt)   throws { /* ignore */ }
	mutating func encode(_ value: UInt8)  throws { /* ignore */ }
	mutating func encode(_ value: UInt16) throws { /* ignore */ }
	mutating func encode(_ value: UInt32) throws { /* ignore */ }
	mutating func encode(_ value: UInt64) throws { /* ignore */ }

	mutating func encode<T>(_ value: T) throws where T : Encodable
	{
		guard let embeddableValue = value as? TOMLikeEmbeddable else {
			throw EncodingError.invalidValue(value, EncodingError.Context(codingPath: codingPath,
				debugDescription: "Single-value encoding is only supported for TOMLikeEmbeddable types."))
		}
		try archive.encode(embedded: embeddableValue.tomlikeEmbedded(), for: codingPath)
	}

}
