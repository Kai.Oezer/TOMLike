// Copyright 2022-2024 Kai Oezer

import Foundation
import IssueCollection

/// A protocol for embedding types that are not supported by TOML.
/// A conforming type is expected to serialize/restore itself to/from String representation.
public protocol TOMLikeEmbeddable
{
	func tomlikeEmbedded() throws -> String

	/// - Parameter tomlikeEmbedded: The text lines, with indexes, from which to restore the TOMLikeEmbeddable instance.
	/// - Parameter issues: An issue collection to which the issues encountered during initialization will be appended.
	init(tomlikeEmbedded : [IndexedLine], issues : inout IssueCollection) throws
}
