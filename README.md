# **TOMLike** is **like TOML**

TOMLike is a native Swift implementation of the textual data serialization format
[TOML](https://toml.io) with certain restrictions that allow encoding/decoding
TOML(ike) content directly from/into a Swift `struct` or `class` that conforms to
the `Codable` protocol.

Key-value pairs in TOML need to map to member variables and their value types in Swift.
Therefore, key names and table names must have a valid [Swift identifier syntax](https://docs.swift.org/swift-book/ReferenceManual/LexicalStructure.html#grammar_identifier)
and can not be chosen freely as in a String-to-String dictionary.

## Embedding custom formats ##

TOMLike allows using a custom, non-TOML text format for serializing parts of your
codable object hierarchy. Types with custom serialization format must implement
the ``TOMLikeEmbeddable`` protocol.

You can still use TOML-style line comments within your custom format. These comments
will removed before your decoder is called. As a consequence, you are not allowed to
use the '#' symbol as part of the syntax of your custom format. 

Your custom format is not allowed to use the TOML table syntax (e.g. "[my.table]").
That is, a line consisting only of a TOML dotted key enclosed in square brackets.
The TOMLike parser partitions the input string into regions based on table headers.
The string provided to your decoder will be the same string that your encoder generated
only if the parser can correctly determine where inside the input string it is embedded.

## Class structure and dependencies ##

![Class structure](Documentation/structure.svg)
